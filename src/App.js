import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import "./App.css";
import LoginForm from "./components/LoginForm/LoginForm";
import { useDispatch } from "react-redux";
import Shop from "./components/Shop/Shop";
import { ProtectedRoute } from "./components/ProtectedRoute";
import { SET_USER, SET_TOKEN } from "./reducers/user";

export default function App() {
  const dispatch = useDispatch();
  const setUser = phoneNumber => dispatch(SET_USER(phoneNumber));
  const setToken = token => dispatch(SET_TOKEN(token));
  try {
    const lstore = localStorage.getItem("loginData");
    if (lstore) {
      setUser(JSON.parse(localStorage.getItem("loginData")).phoneNumber);
      setToken(JSON.parse(localStorage.getItem("loginData")).token);
    }
  } catch (e) {
    return e;
  }
  return (
    <Router>
      <Switch>
        <Route exact path="/login" component={LoginForm} />
        <ProtectedRoute exact path="/" component={Shop} />
        <Route>404</Route>
      </Switch>
    </Router>
  );
}
