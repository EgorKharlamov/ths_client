import React, { Fragment } from "react";

export default props => {
  const handlerOnClickButton = () => {
    localStorage.removeItem("loginData");
    props.history.push("/login");
  };
  return (
    <Fragment>
      <p>hello, im shop</p>
      <button type="button" onClick={() => handlerOnClickButton()}>
        clear token
      </button>
    </Fragment>
  );
};
