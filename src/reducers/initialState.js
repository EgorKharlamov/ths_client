const initialState = {
  user: {
    phoneNumber: '',
    jwt: ''
  },
  order: {
    inProgress: false,
    goodsList: [],
    totalPrice: ''
  }
};
export default initialState;
