import initialState from "./initialState"

export default function userReducer(state=initialState,{type,payload}){
  switch (type) {
    case 'SET_USER':
      return {
        ...state,
        phoneNumber: payload
      }
      case 'SET_TOKEN':
        return {
          ...state,
          jwt: payload
        }
      default:
        return state.user
  }
}

// actions 
export const SET_USER = userPhone => ({
  type: "SET_USER",
  payload: userPhone
});

export const SET_TOKEN = token => ({
  type: "SET_TOKEN",
  payload: token
});
