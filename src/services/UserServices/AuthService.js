const axios = require("axios");

const path = "http://192.168.0.10:4000";

export class Auth {
  static signIn(body) {
    return axios({
      method: "post",
      url: `${path}/users/signin`,
      data: body
    });
  }

  static signUp(body) {
    return axios({
      method: "post",
      url: `${path}/users/signUp`,
      data: body
    });
  }

  static isTokenValid(headers) {
    return axios({
      method: "get",
      url: `${path}/goods`,
      headers: headers
    });
  }
}
