import React, { useState } from "react";
import { useDispatch } from "react-redux";
import s from "./LoginForm.module.sass";
import InputMask from "react-input-mask";
import { SET_USER, SET_TOKEN } from "../../reducers/user";
import { Auth } from "../../services/UserServices/AuthService";

export default props => {
  const dispatch = useDispatch();
  const setUser = phoneNumber => dispatch(SET_USER(phoneNumber));
  const setToken = token => dispatch(SET_TOKEN(token));

  const [phone, onChangePhone] = useState("");
  const [password, onChangePassword] = useState("");
  const [, isSuccessRegister] = useState(false);
  const [phoneErr, changePhoneErr] = useState("");
  const [passwordErr, changePasswordErr] = useState("");

  const handleOnChangeInput = e => {
    if (e.target.name === "phone") onChangePhone(e.target.value);
    if (e.target.name === "password") onChangePassword(e.target.value);
  };

  const handleOnClickSignUp = async () => {
    try {
      isSuccessRegister(false);
      changePhoneErr("");
      changePasswordErr("");

      const phoneToServer = phone.replace(/[- _()+]/g, "");

      const signUpRequest = await Auth.signUp({
        phone: phoneToServer,
        password: password
      });

      if (signUpRequest.data.createdAt) isSuccessRegister(true);
      if (signUpRequest.data.phoneErr) {
        changePhoneErr(signUpRequest.data.phoneErr);
        changePasswordErr("");
      } else if (signUpRequest.data.passwordErr) {
        changePhoneErr("");
        changePasswordErr(signUpRequest.data.passwordErr);
      } else {
        onChangePhone("");
        onChangePassword("");
        changePhoneErr("");
        changePasswordErr("");
      }
    } catch (e) {
      return e;
    }
  };

  const handleOnClickSignIn = async () => {
    try {
      isSuccessRegister(false);
      changePhoneErr("");
      changePasswordErr("");

      const phoneToServer = phone.replace(/[- _()+]/g, "");

      const signInRequest = await Auth.signIn({
        phone: phoneToServer,
        password: password
      });

      if (signInRequest.data.createdAt) isSuccessRegister(true);
      if (signInRequest.data.phoneErr) {
        changePhoneErr(signInRequest.data.phoneErr);
        changePasswordErr("");
      } else if (signInRequest.data.passwordErr) {
        changePhoneErr("");
        changePasswordErr(signInRequest.data.passwordErr);
      } else {
        onChangePhone("");
        onChangePassword("");
        changePhoneErr("");
        changePasswordErr("");

        localStorage.setItem(
          "loginData",
          JSON.stringify({
            token: signInRequest.data.token,
            phoneNumber: signInRequest.data.phoneNumber
          })
        );

        setUser(signInRequest.data.phoneNumber);
        setToken(signInRequest.data.token);
        props.history.push("/");
      }
    } catch (e) {
      return e;
    }
  };

  const showError = () => {
    if (phoneErr) return <p className={s.phoneErr}>{phoneErr}</p>;
    else if (passwordErr) return <p className={s.passwordErr}>{passwordErr}</p>;
    else return null;
  };

  return (
    <div className={s.wrapper}>
      <form className={`${s.form}`}>
        <InputMask
          onChange={e => handleOnChangeInput(e)}
          name="phone"
          value={phone}
          className={`${s.formInput}`}
          type="text"
          placeholder="phone number"
          mask="+7 (999) 999-99-99"
        />
        <input
          onChange={e => handleOnChangeInput(e)}
          name="password"
          value={password}
          className={`${s.formInput}`}
          type="password"
          placeholder="password"
        />
        <div className={`${s.buttonGroup}`}>
          <button
            type="button"
            onClick={() => handleOnClickSignIn()}
            className={`${s.formButton}`}
          >
            Sign In
          </button>

          <button
            type="button"
            onClick={() => handleOnClickSignUp()}
            className={`${s.formButton}`}
          >
            Sign Up
          </button>
        </div>

        {showError()}
      </form>
    </div>
  );
};
