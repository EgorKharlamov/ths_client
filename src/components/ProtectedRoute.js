import React, { useState, useEffect } from "react";
import { Route, Redirect, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { SET_TOKEN } from "../reducers/user";
import { Auth } from "../services/UserServices/AuthService";

export const ProtectedRoute = ({ component: Component, ...rest }) => {
  const dispatch = useDispatch();
  const setToken = token => dispatch(SET_TOKEN(token));
  const [tokenValid, isTokenValid] = useState(true);
  const token = useSelector(state => state.user.jwt);
  let history = useHistory();
  useEffect(() => {
    const checkToken = async token => {
      const res = await Auth.isTokenValid({
        Authorization: `token ${token}`,
        "Content-Type": "application/json"
      });

      if (res.data === "invalid token") {
        localStorage.removeItem("loginData");
        setToken("");
        isTokenValid(false);
        history.push("/login");
      } else {
        isTokenValid(true);
      }
    };
    checkToken(token);
  });

  return (
    <Route
      {...rest}
      render={props => {
        if (tokenValid) {
          return <Component {...props} />;
        } else {
          return <Redirect to="/login" />;
        }
      }}
    />
  );
};
